﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float speed = 30;

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        float y = hitFactor(
            transform.position,
            collision.transform.position,
            collision.collider.bounds.size.y
        );

        if (collision.gameObject.name == "RacketLeft") {
            bounceRacket(1, y);
        } else if (collision.gameObject.name == "RacketRight") {
            bounceRacket(-1, y);
        }
    }

    float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight)
    {
        return (ballPos.y - racketPos.y) / racketHeight;
    }

    void bounceRacket(float direction, float hitFactor = 1)
    {
        Vector2 dir = new Vector2(1, hitFactor).normalized;
        GetComponent<Rigidbody2D>().velocity = dir * speed;
    }
}
